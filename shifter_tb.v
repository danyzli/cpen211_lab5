// Shifter Module Testbench
//--------------------------------------------------------------------------------------------------------------
// Written By: Daniel Li
// Purpose: For testing the shifter module
//==============================================================================================================

module shifter_tb();
	reg [1:0] shift_test; // Dummy shift signal
	reg [15:0] ain_test; // Dummy input signal
	wire [15:0] bout_test; // Dummy output signal
	reg err; // Error indicator

	shifter #(16) DUT ( .shift( shift_test ) , .in( ain_test ) , .sout( bout_test ) );

	// Start test
	initial begin
		err = 1'b0; // initialize to 0
		ain_test = 16'b1111_0000_1100_1111; // Set ain to example in handout

		shift_test = 2'b00;
		#10;
		if (bout_test !== 16'b1111_0000_1100_1111) begin // First operation in handout
			err = 1'b1; // If output unexpected, set error indicator to 1
		end
		//$display("Output is %b, expected %b", bout_test, 16'b1111_0000_1100_1111);
		
		shift_test = 2'b01;
		#10;
		if (bout_test !== 16'b111_0000_1100_11110) begin // Second operation in handout
			err = 1'b1; // If output unexpected, set error indicator to 1
		end
		//$display("Output is %b, expected %b", bout_test, 16'b111_0000_1100_11110);

		shift_test = 2'b10;
		#10;
		if (bout_test !== 16'b01111_0000_1100_111) begin // Third operation in handout
			err = 1'b1; // If output unexpected, set error indicator to 1
		end
		//$display("Output is %b, expected %b", bout_test, 16'b01111_0000_1100_111);

		shift_test = 2'b11;
		#10;
		if (bout_test !== 16'b11111_0000_1100_111) begin // Fourth operation in handout
			err = 1'b1; // If output unexpected, set error indicator to 1
		end
		//$display("Output is %b, expected %b", bout_test, 16'b11111_0000_1100_111);
		if (err == 1'b0) begin
			$display("All tests passed..."); // If all outputs expected, show passed test
		end
	end
endmodule