// Register File Module Testbench
//--------------------------------------------------------------------------------------------------------------
// Written By: Daniel Li
// Purpose: For testing register file module
//==============================================================================================================

// Definitions of test numbers
`define test_number0 16'b0000_0000_0000_0000
`define test_number1 16'b0000_0000_0000_0001
`define test_number2 16'b0000_0000_0000_0010
`define test_number3 16'b0000_0000_0000_0011
`define test_number4 16'b0000_0000_0000_0100
`define test_number5 16'b0000_0000_0000_0101
`define test_number6 16'b0000_0000_0000_0110
`define test_number7 16'b0000_0000_0000_0111

// Definitions of one-hot codes
`define oh0 8'b0000_0001
`define oh1 8'b0000_0010
`define oh2 8'b0000_0100
`define oh3 8'b0000_1000
`define oh4 8'b0001_0000
`define oh5 8'b0010_0000
`define oh6 8'b0100_0000
`define oh7 8'b1000_0000

// Definitions of binary codes
`define bi0 3'b000
`define bi1 3'b001
`define bi2 3'b010
`define bi3 3'b011
`define bi4 3'b100
`define bi5 3'b101
`define bi6 3'b110
`define bi7 3'b111

module regfile_tb();

	// Dummy inputs
	reg [15:0] data_in_test;
	reg [2:0] writenum_test, readnum_test;
	reg write_test, clk_test;

	// Dummy output
	wire [15:0] data_out_test;

	// Self-checking error indicator
	reg err;

	// Instantiate the device under test (DUT)
	regfile DUT ( .data_in(data_in_test), .writenum(writenum_test), .write(write_test), .readnum(readnum_test), .clk(clk_test), .data_out(data_out_test));

	// Task for checking that everything within the DUT is just as expected
	task checkValues;
		input [7:0] expected_decoded_writenum; // expected output of WriteNumDecode
		input [7:0] expected_decoded_readnum; // expected output of ReadNumDecode
		input [15:0] expected_R0; // expected output of wire R0
		input [15:0] expected_R1; // expected output of wire R1
		input [15:0] expected_R2; // expected output of wire R2
		input [15:0] expected_R3; // expected output of wire R3
		input [15:0] expected_R4; // expected output of wire R4
		input [15:0] expected_R5; // expected output of wire R5
		input [15:0] expected_R6; // expected output of wire R6
		input [15:0] expected_R7; // expected output of wire R7
		input [15:0] expected_data_out; // expected output for regfile

		// Begin task
		begin
			err = 1'b0; // first set err back to 0
			// If present decoded_writenum bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.decoded_writenum !== expected_decoded_writenum ) begin
				$display("ERR: decoded_writenum is %b, expected %b", regfile_tb.DUT.decoded_writenum, expected_decoded_writenum);
				err = 1'b1;
			end
			// If present decoded_readnum bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.decoded_readnum !== expected_decoded_readnum ) begin
				$display("ERR: decoded_readnum is %b, expected %b", regfile_tb.DUT.decoded_readnum, expected_decoded_readnum);
				err = 1'b1;
			end
			// If present R0 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R0 !== expected_R0 ) begin
				$display("ERR: R0 is %b, expected %b", regfile_tb.DUT.R0, expected_R0);
				err = 1'b1;
			end
			// If present R1 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R1 !== expected_R1 ) begin
				$display("ERR: R1 is %b, expected %b", regfile_tb.DUT.R1, expected_R1);
				err = 1'b1;
			end
			// If present R2 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R2 !== expected_R2 ) begin
				$display("ERR: R2 is %b, expected %b", regfile_tb.DUT.R2, expected_R2);
				err = 1'b1;
			end
			// If present R3 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R3 !== expected_R3 ) begin
				$display("ERR: R3 is %b, expected %b", regfile_tb.DUT.R3, expected_R3);
				err = 1'b1;
			end
			// If present R4 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R4 !== expected_R4 ) begin
				$display("ERR: R4 is %b, expected %b", regfile_tb.DUT.R4, expected_R4);
				err = 1'b1;
			end
			// If present R5 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R5 !== expected_R5 ) begin
				$display("ERR: R5 is %b, expected %b", regfile_tb.DUT.R5, expected_R5);
				err = 1'b1;
			end
			// If present R6 bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R6 !== expected_R6 ) begin
				$display("ERR: R6 is %b, expected %b", regfile_tb.DUT.R6, expected_R6);
				err = 1'b1;
			end
			// If present R7Q bus is not expected, print situation and set err = 1
			if( regfile_tb.DUT.R7 !== expected_R7 ) begin
				$display("ERR: R7 is %b, expected %b", regfile_tb.DUT.R7, expected_R7);
				err = 1'b1;
			end
			// If present data_out bus is not expected, print situation and set err = 1
			if( data_out_test !== expected_data_out ) begin
				$display("ERR: data_out is %b, expected %b", data_out_test, expected_data_out);
				err = 1'b1;
			end
			// Print message to indicate no errors
			if ( err == 1'b0 ) begin
				$display("Passed test; decoded_readnum, decoded_writenum, registers, and data_out are expected.");
			end
		end
	endtask

	// Task to assign registers values
	task setRegisters;
		input [7:0] desired_writenum_test;
		input [15:0] test_number;

		// Begin task
		begin
			write_test = 1'b0; // Prevent writing while parameters are being changed
			data_in_test = test_number; // Change data_in parameter
			writenum_test = desired_writenum_test; // Change writenum parameter
			write_test = 1'b1; // Start writing
			#10; // Wait 10 time steps so that clk will allow changes to come into effect
			write_test = 1'b0; // Stop writing
		end
	endtask


	// Start the forever looping clock signal
	initial begin
		clk_test = 0; #5; // Start clock as 0
		forever begin // Every 5 time steps, clk changes from 0 to 1 or back
			clk_test = 1; #5;
			clk_test = 0; #5;
		end
	end

	// Start the test bench
	initial begin

		// Start of with no indicated erros
		err = 1'b0;
		
		//===========================================================================================
		// Test 1: Resetting
		//-------------------------------------------------------------------------------------------
		
		// Test starts
		$display("Test 1: Resetting ...");

		readnum_test = `bi0; // We won't be reading anything this test, but here is a dummy value

		// Reset values to 0; test starts at 5 time steps
		setRegisters( `bi0 , `test_number0 );
		#10;
		setRegisters( `bi1 , `test_number0 );
		#10;
		setRegisters( `bi2 , `test_number0 );
		#10;
		setRegisters( `bi3 , `test_number0 );
		#10;
		setRegisters( `bi4 , `test_number0 );
		#10;
		setRegisters( `bi5 , `test_number0 );
		#10;
		setRegisters( `bi6 , `test_number0 );
		#10;
		setRegisters( `bi7 , `test_number0 );
		#10;

		// Test reset complete
		readnum_test = `bi0; // We won't be reading anything this test, but here is a dummy value
		#10;
		checkValues( `oh7 , `oh0 , `test_number0 , `test_number0 , `test_number0 , `test_number0 , `test_number0 , `test_number0 , `test_number0 , `test_number0 , `test_number0);
		// Checks for expected outcomes
		
		$display("Test 1 complete ...");

		//=========================================================================================

		#10;

		//=========================================================================================
		// Test 2: Setting different values to each register and reading each of them
		//-----------------------------------------------------------------------------------------

		// Test starts
		$display("Test 2: Setting different values to each register and reading each of them ... ");

		// Set R0 to 0, R1 to 1, and so on
		setRegisters( `bi0 , `test_number0 );
		#10;
		setRegisters( `bi1 , `test_number1 );
		#10;
		setRegisters( `bi2 , `test_number2 );
		#10;
		setRegisters( `bi3 , `test_number3 );
		#10;
		setRegisters( `bi4 , `test_number4 );
		#10;
		setRegisters( `bi5 , `test_number5 );
		#10;
		setRegisters( `bi6 , `test_number6 );
		#10;
		setRegisters( `bi7 , `test_number7 );
		#10;

		// Test values
		readnum_test = `bi0; // Test reading from first register
		#10;
		checkValues( `oh7 , `oh0, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number0 );
		readnum_test = `bi1; // Test reading from second register
		#10;
		checkValues( `oh7 , `oh1, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number1 );
		readnum_test = `bi2; // Test reading from third register
		#10;
		checkValues( `oh7 , `oh2, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number2 );
		readnum_test = `bi3; // Test reading from fourth register
		#10;
		checkValues( `oh7 , `oh3, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number3 );
		readnum_test = `bi4; // Test reading from fifth register
		#10;
		checkValues( `oh7 , `oh4, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number4 );
		readnum_test = `bi5; // Test reading from sixth register
		#10;
		checkValues( `oh7 , `oh5, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number5 );
		readnum_test = `bi6; // Test reading from seventh register
		#10;
		checkValues( `oh7 , `oh6, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number6 );
		readnum_test = `bi7; // Test reading from eighth register
		#10;
		checkValues( `oh7 , `oh7, `test_number0 , `test_number1 , `test_number2 , `test_number3 , `test_number4 , `test_number5 , `test_number6 , `test_number7 , `test_number7 );

		$display("Test 2 complete ... ");

		//=======================================================================================

		#10;

		//=======================================================================================
		// Test 3: Overwriting registers values and reading each of them
		//---------------------------------------------------------------------------------------

		// Test starts
		$display("Test 3: Overwriting registers values and reading each of them ... ");

		// Set R0 to 7, R1 to 6, and so on
		setRegisters( `bi0 , `test_number7 );
		#10;
		setRegisters( `bi1 , `test_number6 );
		#10;
		setRegisters( `bi2 , `test_number5 );
		#10;
		setRegisters( `bi3 , `test_number4 );
		#10;
		setRegisters( `bi4 , `test_number3 );
		#10;
		setRegisters( `bi5 , `test_number2 );
		#10;
		setRegisters( `bi6 , `test_number1 );
		#10;
		setRegisters( `bi7 , `test_number0 );
		#10;

		// Test values
		readnum_test = `bi0; // Test reading from first register
		#10;
		checkValues( `oh7 , `oh0, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number7 );
		readnum_test = `bi1; // Test reading from second register
		#10;
		checkValues( `oh7 , `oh1, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number6 );
		readnum_test = `bi2; // Test reading from third register
		#10;
		checkValues( `oh7 , `oh2, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number5 );
		readnum_test = `bi3; // Test reading from fouth register
		#10;
		checkValues( `oh7 , `oh3, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number4 );
		readnum_test = `bi4; // Test reading from fifth register
		#10;
		checkValues( `oh7 , `oh4, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number3 );
		readnum_test = `bi5; // Test reading from sixth register
		#10;
		checkValues( `oh7 , `oh5, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number2 );
		readnum_test = `bi6; // Test reading from seventh register
		#10;
		checkValues( `oh7 , `oh6, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number1 );
		readnum_test = `bi7; // Test reading from eighth register
		#10;
		checkValues( `oh7 , `oh7, `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number0 );

		$display("Test 3 complete ... ");

		//===================================================================================

		#10;

		//===================================================================================
		// Test 4: Attempting to overwrite without "write" signal set to "high"
		//-----------------------------------------------------------------------------------

		// Test starts
		$display("Test 4: Attempting to overwrite without 'write' signal set to 'high'");

		// Attempt to write 0 to all registers while 'write' signal is off
		write_test = 1'b0;
		data_in_test = `test_number0;

		writenum_test = `bi0;
		#10;
		writenum_test = `bi1;
		#10;
		writenum_test = `bi2;
		#10;
		writenum_test = `bi3;
		#10;
		writenum_test = `bi4;
		#10;
		writenum_test = `bi5;
		#10;
		writenum_test = `bi6;
		#10;
		writenum_test = `bi7;
		#10;

		// Test values
		readnum_test = `bi0; // Test reading from first register
		#10;
		checkValues( `oh7 , `oh0 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number7 );
		readnum_test = `bi1; // Test reading from second register
		#10;
		checkValues( `oh7 , `oh1 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number6 );
		readnum_test = `bi2; // Test reading from third register
		#10;
		checkValues( `oh7 , `oh2 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number5 );
		readnum_test = `bi3; // Test reading from fouth register
		#10;
		checkValues( `oh7 , `oh3 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number4 );
		readnum_test = `bi4; // Test reading from fifth register
		#10;
		checkValues( `oh7 , `oh4 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number3 );
		readnum_test = `bi5; // Test reading from sixth register
		#10;
		checkValues( `oh7 , `oh5 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number2 );
		readnum_test = `bi6; // Test reading from seventh register
		#10;
		checkValues( `oh7 , `oh6 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number1 );
		readnum_test = `bi7; // Test reading from eighth register
		#10;
		checkValues( `oh7 , `oh7 , `test_number7 , `test_number6 , `test_number5 , `test_number4 , `test_number3 , `test_number2 , `test_number1 , `test_number0 , `test_number0 );

		$display("Test 4 complete ... ");

		//$stop;
		#10000;
	end
endmodule