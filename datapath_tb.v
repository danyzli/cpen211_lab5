//--------------------------------------------------------------------------------
// Testbench module for the datapath
// Author: Warren Chan Wan Fong
// Purpose: Testing if everything works up to expectations in the datapath module
//--------------------------------------------------------------------------------


module datapath_tb ();
// declaration of the inputs of the datapath so we can monitor them and change them in the initial block
  reg [15:0] datapath_in;
  reg [2:0] writenum; // which register to write to
  reg [2:0] readnum;  // which register to read from 
  reg clk , write;
  reg err;
  reg vsel, asel, bsel , loada, loadb , loadc, loads; // loads for the multiplexers and the pipeline registers
  reg [1:0] ALUop; // what operation should the ALU perform
  reg [1:0] shift; // what operation should the shifter perform
  reg [15:0] altoptA; // note that we do not need to include altoptB because it is determined combinationally within the datapath module itself

// declarations of the wires connected to the outputs of the datapath, so that we can monitor them and check if we are getting what we expected values
  wire [15:0] datapath_out;
  wire Z_out;

  // initial script responsible for changing the clock input every 10 times steps
  initial begin
    clk = 0;  //inially, the clock is set to be 0, because we will be looking for a rising edge
    #5;

    forever begin //This block will keep on running until we hit the $stop in the second initial script
      clk = 1; // This provides a rising edge of the clock, on which the vDFF will update its output
      #5;
      clk = 0; // This provides a falling edge of the clock, that is setting clock to zero so we can have another rising edge.
      #5;
    end
  end

  // task used to self check the module within the testbench
  task datapath_checker;
    input [15:0] expected_datapath_out;
    begin 
      if (datapath_tb.DUT.datapath_out !== expected_datapath_out) begin
      	$display ("Error!!!! Output of Datapath is %b, we expected %b", datapath_tb.DUT.datapath_out , expected_datapath_out);
      	err = 1;
      end
      else begin 
        $display ("Output is %b, we were expecting %b", datapath_tb.DUT.datapath_out , expected_datapath_out);
      end
    end
  endtask




  // instantiate the datapath module
  datapath DUT (.clk(clk),
  				.readnum(readnum),
  				.vsel(vsel),
  				.loada(loada),
  				.loadb(loadb),
  				.shift(shift),
  				.asel(asel),
  				.bsel(bsel),
  				.ALUop(ALUop),
  				.loadc(loadc),
  				.loads(loads),
  				.writenum(writenum),
  				.write(write),
  				.datapath_in(datapath_in),
  				.Z_out(Z_out),
  				.datapath_out(datapath_out) );


// first testing set: 
// 1. take the absolute number 7 and store it in R0 in the register file.
// 2. Take the absolute number 2 and store it in R1 in the register file.
// 3. Store the value (R1 + (R0 shifted left by 1)) in the register 2. I believe that this should be computed and outputed through datapath_out,
//    and simulatneously fed into the writeback multiplexer to be written in the register R2 
  
// step one: datapath in = 

  initial begin 
  	// These two loads are set to 0 so that we use the stored values to feed to the ALU
  	asel = 1'b0;
  	bsel = 1'b0;

    datapath_in = 16'b0000_0000_0000_0111; // we want to write the number 7
    vsel = 1'b1; // we want to pass the datapath_in value as the input to the regfile
    writenum = 3'b000; // we want to write to register R0
    write = 1'b1; // indicates that we want to write to the register file
    #10; // wait until after the rising edge of the clock for changes to have happened

    datapath_in = 16'b0000_0000_0000_0010; // we want to write the number 2
    writenum = 3'b001; //we want to write to register R1
    write = 1'b1; // indicates that we want to write to the register file
    #10;
    // At this point, we should have 7 in R0 and 2 in R1

    write = 1'b0; // at this point we do not want to be writing to the register file
    readnum = 3'b000; // we read the data in R0;
    loadb = 1'b1; // we let pipeline register A update to the value that we are currently reading
    #10; // wait for next rising edge to update contents of B

    readnum = 3'b001; // we read the data in R1
    loadb = 1'b0; // we have already put a value in A. We don't want the register to update to another value yet.
    loada = 1'b1; // we let pipeline register B update to the value that we are currently reading
    #10; // wait for next rising edge to update contents of A

    loadb = 1'b0; // we have already put a value in B. We don't want the register to update to another value yet.
    shift = 2'b01; // left shift by one 
    ALUop = 2'b00; // addition of inputs to ALU
    loadc = 1'b1; // set to 1 so that the value of pipelinereg C is updated on rising edge of clock
    loads = 1'b1; // set to 1 so that the status load enabled register is updated on rising edge of clock
    #10

    datapath_checker(16'b0000_0000_0001_0000); // we check that we are outputing the correct value, which is 16. We still need to write it back in the register file

    vsel = 1'b0; // we want to pass the value of datapath_out as input data_in to the regfile
    writenum = 3'b010; // we want to store the computed value in R2
    write = 1'b1; // indicates we want to write to the register file
    #10; 

    //to check that we obtained the right value in the register R2, we can read from it, and look at the data_out signal in our DUT
    write = 1'b0; // we do not want to write to the register file at the moment
    readnum = 3'b010; // indicates we want to read from R2. data_out signal will then be the value being stored in R2
    //note that reading from the regfile is not synchornous with the clock, which means that the value of data_out should already be the value in R2 at this point.
    #10; // let's wait nonetheless lol

    $display("The value stored in R2 is %b, we expected %b", datapath_tb.DUT.data_out , 16'b0000_0000_0001_0000 );

    //$stop;
    #10000;
  end // end of testscript


  endmodule
  

