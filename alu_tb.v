//Contains the testbench module for the ALU module found in the alu.v file


  `define SW 16

  module ALU_tb();
    reg [15:0] ain, bin;  // reg so that it can be modified in the initial block
    reg [1:0] selection;  //  reg so that it can be modified in the initial block
    wire [15:0] out;
    wire Z;
    reg err; // error signal which is used in the task to detect as from where we get undesirable output

    ALU DUT (ain, bin, selection , out , Z);  // instantiation of the device under test

    //selfchecking testbench format, using a task for self checking
    task alu_checker;
      input [`SW:0] expected_output;
    begin 
      if (ALU_tb.DUT.out !== expected_output) begin
      	$display ("Error!! Ouput of ALU is %b, expected output is %b", ALU_tb.DUT.out , expected_output);
      	err = 1'b1; // This signal will get a value of one and keep it thereafter if the output is not what we expected.
      end
    end
    endtask


    // selfchecking task for the output to the status register Z.
    task z_checker;
      input expected_status;
    begin 
      if(ALU_tb.DUT.Z !== expected_status) begin
        $display("Error!! The status is %b , we were expecting %b", ALU_tb.DUT.Z , expected_status);
        err = 1'b1;
      end
    end
    endtask    

    //The test script for the ALU starts here
    initial begin 

      $display("Checking for the addition operand, selection 00");
      ain = 16'b0000_0000_0000_1111;
      bin = 16'b0000_0000_0000_1111;
      selection = 2'b00;
      #5;
      alu_checker(16'b0000_0000_0001_1110);
      z_checker(1'b0);

      ain = 16'b0000_0000_0000_0000;
      bin = 16'b0000_0000_0000_0000;
      #5;
      alu_checker(16'b0000_0000_0000_0000);
      z_checker(1'b1);

      ain = 16'b1111_0000_1111_0000;
      bin = 16'b0000_1111_0000_1111;
      #5;
      alu_checker(16'b1111_1111_1111_1111);
      z_checker(1'b0);

      ain = 16'b1010_0101_1010_0000;
      bin = 16'b0101_1010_0101_1111;
      #5;
      alu_checker(16'b1111_1111_1111_1111);
      z_checker(1'b0);

      ain = 16'b1111_0000_1111_0000;
      bin = 16'b0000_1111_0000_1111;
      #5;
      alu_checker(16'b1111_1111_1111_1111);
      z_checker(1'b0);

      $display("Check for addition (selection 00) completed");

      $display("Checking for the substraction operand");

      ain = 16'b1111_0000_1111_0000;
      bin = 16'b0000_1111_0000_1111;
      selection = 2'b01;
      #5;
      alu_checker(16'b1110_0001_1110_0001);
      z_checker(1'b0);

      ain = 16'b1000_0000_0000_0000;
      bin = 16'b0100_0000_0000_0000;
      #5;
      alu_checker(16'b0100_0000_0000_0000);
      z_checker(1'b0);

      ain = 16'b1000_0000_0000_0000;
      bin = 16'b1000_0000_0000_0000;
      #5;
      alu_checker(16'b0000_0000_0000_0000);
      z_checker(1'b1);

      $display("Check for substraction (selection 01) completed");

      $display ("Checking for AND operand");

      ain = 16'b1000_0000_0000_0000;
      bin = 16'b1000_0000_0000_0000;
      selection = 2'b10;
      #5;
      alu_checker(16'b1000_0000_0000_0000);
      z_checker(1'b0);

      ain = 16'b0000_0000_0000_0000;
      bin = 16'b1111_1111_1111_1111;
      #5;
      alu_checker(16'b0000_0000_0000_0000);
      z_checker(1'b1);

      ain = 16'b1010_0110_0010_0100;
      bin = 16'b1010_0100_1001_0100;
      #5;
      alu_checker(16'b1010_0100_0000_0100);
      z_checker(1'b0);

      ain = 16'b0000_0000_0000_0000;
      bin = 16'b0000_0000_0000_0000;
      #5;
      alu_checker(16'b0000_0000_0000_0000);
      z_checker(1'b1);

      $display("Check for AND operand (selection 10) completed");

      $display("Checking for NOT operation (on Bin)");

      ain = 16'bxxxx_xxxx_xxxx_xxxx;
      bin = 16'b1000_0000_0000_0000;
      selection = 2'b11;
      #5;
      alu_checker(16'b0111_1111_1111_1111);
      z_checker(1'b0);

      bin = 16'b1111_1111_1111_1111;
      #5;
      alu_checker(16'b0000_0000_0000_0000);
      z_checker(1'b1);

      bin = 16'b1010_0101_0110_0111;
      #5;
      alu_checker(16'b0101_1010_1001_1000);
      z_checker(1'b0);

      $display("Check for NOT operation (selection 11) completed");

      if (err !== 1'b1) begin
        $display("All checks completed, ALU is operational");
      end



      //stop;
      #10000;
    end

  endmodule