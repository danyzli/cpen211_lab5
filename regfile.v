// Register File Module
//--------------------------------------------------------------------------------------------------------------
// Written By: Daniel Li
// Purpose: The register file stores values in registers 0 to 7 and allows the user to write and read from them
//==============================================================================================================


// Definitions of one-hot codes
`define oh0 8'b0000_0001
`define oh1 8'b0000_0010
`define oh2 8'b0000_0100
`define oh3 8'b0000_1000
`define oh4 8'b0001_0000
`define oh5 8'b0010_0000
`define oh6 8'b0100_0000
`define oh7 8'b1000_0000

// Definitions of binary codes
`define bi0 3'b000
`define bi1 3'b001
`define bi2 3'b010
`define bi3 3'b011
`define bi4 3'b100
`define bi5 3'b101
`define bi6 3'b110
`define bi7 3'b111

// 3:8 decoder module
module decoder( binary_in, onehot_out );
	parameter n = 3;
	parameter m = 8;
	input [n-1:0] binary_in; // 3 bit binary in signal
	output [m-1:0] onehot_out; //  8 bit one-hot output

	wire [m-1:0] onehot_out = 1 << binary_in;
endmodule

// Wide MUX module
module MUX8( ain, bin, cin, din, ein, fin, gin, hin, select, out );
	parameter n = 16;
	input [n-1:0] ain, bin, cin, din, ein, fin, gin, hin;
	input [7:0] select; // one-hot select signal
	output [n-1:0] out;

	wire [n-1:0] out = ( {n{select[0]}} & ain ) | ( {n{select[1]}} & bin ) | ( {n{select[2]}} & cin ) | ( {n{select[3]}} & din ) | ( {n{select[4]}} & ein ) | ( {n{select[5]}} & fin ) | ( {n{select[6]}} & gin ) | ( {n{select[7]}} & hin );

endmodule

// Register file
module regfile(data_in,writenum,write,readnum,clk,data_out);
	input [15:0] data_in;
	input [2:0] writenum, readnum;
	input write, clk;
	output [15:0] data_out;

	// Internal signal declarations
	wire [7:0] decoded_writenum; // 8-bit one-hot representation of writenum
	wire [7:0] decoded_readnum; // 8-bit one-hot representation of readnum
	wire [15:0] R0; // Wire from R0 to wire MUX
	wire [15:0] R1; // Wire from R1 to wire MUX
	wire [15:0] R2; // Wire from R2 to wire MUX
	wire [15:0] R3; // Wire from R3 to wire MUX
	wire [15:0] R4; // Wire from R4 to wire MUX
	wire [15:0] R5; // Wire from R5 to wire MUX
	wire [15:0] R6; // Wire from R6 to wire MUX
	wire [15:0] R7; // Wire from R7 to wire MUX

	// writenum goes to 3:8 decoder
	decoder #(3,8) WriteNumDecode( .binary_in( writenum ), .onehot_out( decoded_writenum ) );

	// data_in is inputed as 16 bit bus to every D flip flop
	// clk is connected to each D flip flop
	PipeLineReg #(16) Reg0( .clk( clk ), .load( ( decoded_writenum[0] && write ) ), .in( data_in ), .out( R0 ) );
	PipeLineReg #(16) Reg1( .clk( clk ), .load( ( decoded_writenum[1] && write ) ), .in( data_in ), .out( R1 ) );
	PipeLineReg #(16) Reg2( .clk( clk ), .load( ( decoded_writenum[2] && write ) ), .in( data_in ), .out( R2 ) );
	PipeLineReg #(16) Reg3( .clk( clk ), .load( ( decoded_writenum[3] && write ) ), .in( data_in ), .out( R3 ) );
	PipeLineReg #(16) Reg4( .clk( clk ), .load( ( decoded_writenum[4] && write ) ), .in( data_in ), .out( R4 ) );
	PipeLineReg #(16) Reg5( .clk( clk ), .load( ( decoded_writenum[5] && write ) ), .in( data_in ), .out( R5 ) );
	PipeLineReg #(16) Reg6( .clk( clk ), .load( ( decoded_writenum[6] && write ) ), .in( data_in ), .out( R6 ) );
	PipeLineReg #(16) Reg7( .clk( clk ), .load( ( decoded_writenum[7] && write ) ), .in( data_in ), .out( R7 ) );

	// readnum is connected to 3:8 decoder
	decoder #(3,8) ReadNumDecode( .binary_in( readnum ), .onehot_out( decoded_readnum ) );

	// Wide MUX fed with outputs from D flip flops and decoded_readnum
	MUX8 #(16) wideMux( .ain( R0 ), .bin( R1 ), .cin( R2 ), .din( R3 ), .ein( R4 ), .fin( R5 ), .gin( R6 ), .hin( R7 ), .select( decoded_readnum ), .out( data_out ));
	
endmodule