// Arithmetic Logic Unit
// Author: Warren Chan Wan Fong
// Purpose: This module will perform the basic arithmetic on the data path, passing along the processed values to the rest of the path

module ALU (Ain, Bin, ALUop, out, Z);
  input [15:0] Ain, Bin; // These are the two 16 bit numbers on which the module needs to do operations
  input [1:0] ALUop; // contains information about which operation we want to perform on the inputs
  output [15:0] out; // wire containing the result signal
  output Z;
  reg [15:0] result; 
  reg status;

  always @(*) begin
    case ( ALUop )
      2'b00 : result = Ain[15:0] + Bin[15:0]; // Addition of Ain and Bin 
      2'b01 : result = Ain[15:0] - Bin[15:0]; // Ain - Bin
      2'b10 : result = Ain[15:0] & Bin[15:0]; // Ain ANDed with Bin 
      2'b11 : result = ~Bin[15:0];  // not Bin
      // no default needed as we are already covering all the cases
    endcase
  end

  assign out = result; // the wire out gets the value of the result

  always @(*) begin
    case (result)
      16'b0000_0000_0000_0000 : status = 1'b1; // status output to the status register is 1 if the ALU result is exactly 0
      default : status = 1'b0;
    endcase
  end

  assign Z = status;

 endmodule


// Z = ~(result[0] | result[1] | result[2] | result[3] | result[4] | result[5] | result[6] | result[7] | result[8] | result[9] | result[10] | result[11] | result[12] | result[13] | result[14] | result[15] );