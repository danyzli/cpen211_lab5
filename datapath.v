// ----------------------------------------------------------------------------------------------
// datapath module
// Author: Warren Chan Wan Fong
// Date: October 13th 2018
// This module is greatly based on the figure 1 found in the lab handout. Its main purpose is to 
// put together the different modules that were developped and to connect them together so that 
// our RISC machine does what we expect. At this point, we have an operational ALU module, a 
// pipeline register module (with integrated multiplexer), shifter unit. Note that the MUX which
// is found in the pipeline reg module can easily be used again as instantiations for other 
// purposes in the datapath.
//-----------------------------------------------------------------------------------------------

module datapath (clk,	// this will determine the timing at which data is being processed along the path

				 // inputs associated with the writing stage in the regfile
				 readnum,
				 vsel,
				 loada,
				 loadb,

				 // inputs associated with the manipulation of the data obtained from the regfile
				 shift,
				 asel,
				 bsel,
				 ALUop,
				 loadc,
				 loads,

				 // inputs needed when writing in the register file
				 writenum,
				 write,
				 datapath_in,

				 // outputs of the datapath
				 Z_out,
				 datapath_out
				 );

input clk;
input [2:0] readnum; // from which register to read the value
input vsel; // selection of what to feed in the register file as data_in
input loada, loadb, loadc, loads; // load associated with pipeline reg of corresponding letter
input [1:0] shift; // information about how to shift the value stored in pipeline reg B before processing in ALU
input asel; // which to choose in the multiplexer which receives the output of A (this should always be 0 for lab 5)
input bsel; // which to choose in the multiplexer which receives the output of B (this should always be 0 for lab 5)
input [1:0] ALUop; // what operation to perform on the inputs of the ALU
input [2:0] writenum; // in which register we want to write a value
input write; // indicates that we want to write/save in a reg in the register file
input [15:0] datapath_in; // actual number which we want input to the datapath
output Z_out; // this will be a 1 if the result of the ALU is exactly 0. refer to 1.2.7 for more details
output [15:0] datapath_out; // output driven by the ouput of pipeline reg C, which should contain the result from the ALU


wire [15:0] data_in; // wire connecting the writeback multiplexer to the regfile module
wire [15:0] data_out; // wire connecting the regfile to pipeline regs A and B
wire [15:0] output_A; // output from the pipeline reg A
wire [15:0] output_B; // output from the pipeline reg B
wire [15:0] sout; // wire carrying the output from shifter unit to MUX
wire [15:0] Ain; // wire connecting MUX to ALU in A branch
wire [15:0] Bin; // wire connecting MUX to ALU in B branch
wire [15:0] alt_opt_A; // second option in the MUX following the pipeling reg A
wire [15:0] alt_opt_B; // second option in the MUX following the pipeline reg B
wire [15:0] ALU_out; // wire containing the processed value
wire zout; // wire containing the status signal from the ALU to the status register.


Mux2 WritebackMUX (.load(vsel), .Ain(datapath_in), .Bin(datapath_out), .out(data_in)); // instantation of the writeback MUX, which decides whether we input the old output or a new input to the reg file for processing

// module regfile(data_in,writenum,write,readnum,clk,data_out);
regfile REGFILE (.data_in(data_in) , .writenum(writenum) , .write(write),  .readnum(readnum) , .clk(clk) , .data_out(data_out)); //instantiation of the register file

// module PipeLineReg (clk, load, in , out);
PipeLineReg A (.clk(clk) , .load(loada) , .in(data_out) , .out(output_A));
PipeLineReg B (.clk(clk) , .load(loadb) , .in(data_out) , .out(output_B));

assign alt_opt_A = 16'b0000_0000_0000_0000;
// Multiplexer in the branch below A, which chooses what to input to the ALU module
Mux2 mux_A (.load(asel) , .Ain(alt_opt_A) , .Bin(output_A) , .out(Ain));

shifter U1 (.in(output_B), .shift(shift) , .sout(sout));
assign alt_opt_B = {11'b0, datapath_in[4:0]};
// Multiplexer in the branch below B, which chooses what to input to the ALU module
Mux2 mux_B (.load(bsel) , .Ain(alt_opt_B) , .Bin(sout) , .out(Bin));


ALU alu (.Ain(Ain) , .Bin(Bin) , .ALUop(ALUop) , .out(ALU_out) , .Z(zout));


PipeLineReg #(1) Status (.clk(clk) , .load(loads) , .in(zout) , .out(Z_out)); // Note the parametization #(1)


PipeLineReg C (.clk(clk) , .load(loadc) , .in(ALU_out) , .out(datapath_out));

endmodule


// // NOTE: the MUX2 can also be found in the Pipelinereg.v file (exact same copy)
// // Multiplexer module to choose between two options.
// // Author: Warren Chan Wan Fong
// // Purpose: provides the ability to select one of several options, by comparison with a load. Note that this is pure combinational logic

// module Mux2 (load, Ain, Bin, out);
//   parameter n = 16; // set to 16 so that we can accomodate 16 bit signals by default
//   input [n-1:0] Ain; // option to be selected if the load is one
//   input [n-1:0] Bin; // option to be selected if the load is zero
//   input load; // We choose which option to pass on depending on the load.
//   output [n-1:0] out; // this is the output from the MUX, not the be confused with the output from the PipelineReg itself.
//   wire [n-1:0] out;

//   assign out = load ? Ain : Bin; // translates as: if load is 1, then choose Ain, if load is zero, then choose Bin.

// endmodule


// Pipeline Registers
// Purpose: These are load enabled registers which hold the values A, B and C. They are used to 
//          hold the values in the datapath. Note that they do not depend only the clock, but rather
//          on the load. For instance, when we want to read a value from the register file to continue on the data path.
//          The register to read from is selected using binary code readnum, which is decoded to a one hot code passed which
//          is passed to the MUX. The value stored in the selected register is thus driven to data_out and that value is stored
//          in either A or B when their respective loads are 1 and the clock is 1. While the load is 0, the value stored in the
//          pipeline registers will just remain the previous output of the vDFF. In other words, the MUX is made to choose what 
//          is being fed as input to the vDFF. If the load is 1, then the new input is given to the vDFF which will update its 
//          output value on the rising edge of the clock. When the load is 0 however, the previous output is fed to the input of
//          the vDFF, thus making it update to the same thing when the clock has a rising edge.

module PipeLineReg (clk, load, in , out);
  parameter n = 16; //this parameter needs to be put to 1 when the module is instantiated for the status register, as it is a one bit register with load enable.
  input [n-1:0] in;
  input load; // The load controls what is being fed as input D to the vDFF.
  input clk; // The clock controls when the vDFF has the potential to update. It will however update only if the load on the MUX is 1, else it will keep its old output value.
  output [n-1:0] out; 
  wire [n-1:0] out; /////*********************
  wire [n-1:0] D; 

   Mux2 #(n) Chooser (load, in, out, D); // this instantiation will make the MUX
                                        // choose between in or out, depending on the load,
                                        // and transmit it through the wire D t the vDFF as the input data D

  vDFF #(n) Register (clk, D, out);

 endmodule



// Multiplexer module to choose between two options.
// Author: Warren Chan Wan Fong
// Purpose: provides the ability to select one of several options, by comparison with a load. Note that this is pure combinational logic

module Mux2 (load, Ain, Bin, out);
  parameter n = 16;
  input [n-1:0] Ain; // option to be selected if the load is one
  input [n-1:0] Bin; // option to be selected if the load is zero
  input load; // We choose which option to pass on depending on the load.
  output [n-1:0] out; // this is the output from the MUX, not the be confused with the output from the PipelineReg itself.
  wire [n-1:0] out;

  assign out = load ? Ain : Bin; // translates as: if load is 1, then choose Ain, if load is zero, then choose Bin.

endmodule


// /////////////////////////////// NOTE this module of vDFF was included here just to be able to test the pipeline reg. It needs to be deleted as it is already found in the lab5top.v
// module vDFF(clk,D,Q);
//   parameter n=1;
//   input clk;
//   input [n-1:0] D;
//   output [n-1:0] Q;
//   reg [n-1:0] Q;

//   always @(posedge clk)
//     Q <= D;
// endmodule



// //Testing the implementation of the register file with load enable.

// module pipeline_tb ();
//   reg [15:0] ain;
//   wire [15:0] out;
//   reg load;  // load is set as a reg type as it will be modified in an initial block
//   reg clock; // clock is set as a reg type as it will be modified in an initial block
//   reg err;
  
//   // Instantiate the pipelinereg
//   PipeLineReg #(16) DUT (clock, load, ain , out);


//   task pipelineregChecker;
//     input [15:0] expected_output;
//     begin 
//       if (pipeline_tb.DUT.out !== expected_output) begin
//         $display ("Error!! Ouput of PipeLineReg is %b, expected output is %b", pipeline_tb.DUT.out , expected_output);
//         err = 1'b1; // This signal will get a value of one and keep it thereafter if the output is not what we expected.
//       end
//     end
//     endtask


//  //initial script responsible for changing the clock input every 10 times steps
//   initial begin
//     clock = 0;  //inially, the clock is set to be 0, because we will be looking for a positive edge
//     #5;

//     forever begin //This block will keep on running until we hit the $stop in the second initial script
//       clock = 1; // This provides a rising edge of the clock, on which the vDFF will update its output
//       #5;
//       clock = 0; // This provides a falling edge of the clock, that is setting clock to zero so we can have another rising edge.
//       #5;
//     end
//   end



//   initial begin 
//     ain = 16'b0000_1111_0000_1111;
//     load = 1'b1;
//     #10;
//     pipelineregChecker(16'b0000111100001111); // we expect the input to have been updated to the output sa load is 1;

//     ain = 16'b0000_0000_0000_0000;
//     load = 1'b0;
//     #10;
//     pipelineregChecker(16'b0000111100001111); // we are still expecting the old output as the load was set to 0;

//     ain = 16'b1101_1010_0110_1001;
//     load = 1'b1;
//     #10;
//     pipelineregChecker(16'b1101101001101001); // now that the load is one, we are expecting the ain to be the output of the register.


//     $stop;
//   end
// endmodule
