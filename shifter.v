// Shifter Module
//--------------------------------------------------------------------------------------------------------------
// Written By: Daniel Li
// Purpose: The shifter allows easy multiplication by 2 by shifting the bits of busses
//==============================================================================================================

module shifter ( in , shift, sout );
	parameter n = 16;
	input [1:0] shift; // what kind of shift we want to perform
	input [n-1:0] in; // input to shift
	output [n-1:0] sout; // output from shifter

	reg [n-1:0] result; // reg defined because this value is modified in the always block

	always @( shift , in ) begin // everytime that the shift or the input changes, we reevaluate the always block
		case( shift )
			2'b00: result = in;
			2'b01: result = { in[n-2:0] , 1'b0 };
			2'b10: result = { 1'b0 , in[n-1:1] };
			2'b11: result = { 1'b1 , in[n-1:1] };
			// no default needed since all bases covered
		endcase
	end

	assign sout = result; // wire output drives value from reg

endmodule